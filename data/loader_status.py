import json
import os


# Helpers for the state file to only use sets
class jsonEncoder(json.JSONEncoder):
    def default(self, obj):
        return list(obj)


class loader:
    file_path = "loader_state.json"

    def __init__(self):
        self.data = {}

        # loading state from a file if exists
        if os.path.exists(loader.file_path):
            with open(loader.file_path, "r") as json_file:
                self.data = json.load(json_file, object_hook=self._load_data)

    def get_gsym_data(self, gsym):
        if gsym not in self.data:
            self.data[gsym] = {
                "completed_days": set(),
                "failed_symbols": {},
                "completed_symbols": {},
            }
        return self.data[gsym]

    # Completed days
    def add_completed_day(self, gsym, date):
        d = self.get_gsym_data(gsym)
        d["completed_days"].add(str(date))

    # Symbol
    def add_failed_symbol(self, gsym, date, symbol):
        d = self.get_gsym_data(gsym)
        date_s = str(date)
        if date_s not in d["failed_symbols"]:
            d["failed_symbols"][date_s] = set()
        d["failed_symbols"][date_s].add(symbol)
        self._remove_symbol_from_completed(gsym, date, symbol)

    def add_completed_symbol(self, gsym, date, symbol):
        d = self.get_gsym_data(gsym)
        date_s = str(date)
        if date_s not in d["completed_symbols"]:
            d["completed_symbols"][date_s] = set()
        d["completed_symbols"][date_s].add(symbol)
        self._remove_symbol_from_failed(gsym, date, symbol)

    # Other
    def is_day_failed(self, gsym, date):
        d = self.get_gsym_data(gsym)
        date_s = str(date)
        return date_s in d["failed_symbols"]

    def is_day_completed(self, gsym, date):
        d = self.get_gsym_data(gsym)
        return str(date) in d["completed_days"]

    def save(self):
        with open(loader.file_path, "w") as json_file:
            json.dump(self.data, json_file, cls=jsonEncoder)

    # Helpers
    def _remove_symbol_from_failed(self, gsym, date, symbol):
        if gsym not in self.data:
            return
        date_s = str(date)
        if date_s not in self.data[gsym]["failed_symbols"]:
            return
        if symbol in self.data[gsym]["failed_symbols"][date_s]:
            self.data[gsym]["failed_symbols"][date_s].remove(symbol)

    def _remove_symbol_from_completed(self, gsym, date, symbol):
        if gsym not in self.data:
            return
        date_s = str(date)
        if date_s not in self.data[gsym]["completed_symbols"]:
            return
        if symbol in self.data[gsym]["completed_symbols"][date_s]:
            self.data[gsym]["completed_symbols"][date_s].remove(symbol)

        if date_s in self.data[gsym]["completed_days"]:
            self.data[gsym]["completed_days"].remove(date_s)

    # Helper to load data from json and change lists to sets
    def _load_data(self, dct):
        newD = {}
        for k, v in dct.items():
            if isinstance(v, list):
                v = set(v)
            newD[k] = v
        return newD
