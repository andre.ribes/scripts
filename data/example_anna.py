# Get data from Clickhouse
import datetime

import numpy as np
from octopus import archives, refdata_options

# Get all options refdata
with refdata_options.get_opt_refdata_dao(refdata_options.OptRefdataDao.TABLE_NAME_V2) as dao:
    refdata = dao.get_all_refdata()

# Get options refdata that we are interested with
expiry_date = np.datetime64(datetime.date(2023, 12, 5))
base_asset = "OSPXW"
spxw_refdata_options = refdata[
    (refdata.base_asset == base_asset) & (refdata.expiry_date == expiry_date)
]
print(f"Number of options: {len(spxw_refdata_options.index)}")

times_begin = (
    spxw_refdata_options.expiry_timestamp[0].replace(hour=0, minute=0, second=0).tz_localize("UTC")
)
times_end = spxw_refdata_options.expiry_timestamp[0].tz_localize("UTC")
db_archives = archives.ClickArchive("realtime.market_data")
option_dataframe = db_archives.load_symbols_between_times(
    symbols=spxw_refdata_options.symbol.unique(),
    begin=times_begin,
    end=times_end,
    columns=["ask", "bid", "mid", "ask_size", "bid_size", "volume", "source", "env"],
)
print(f"Option dataframe is length {len(option_dataframe)}")
