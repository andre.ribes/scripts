import argparse
import datetime
import json
import os
import pathlib
import sys
import time
import traceback

import helpers
import pytz
from epoch_data import athena, calendarlib
from epoch_infra import commonlib
from epoch_markets import symlib
from epoch_markets.mdcatalog.options import data_config
from octopus import clickhouse

commonlib.init_logging("DEBUG")

# Athena Framework configuration
ATHENA_DATA_CONTEXT = athena.DataContext(
    read_namespaces=["test", "prod"], write_namespace="test"
)  # write new cached data to /tmp/test
OPTIONS_CONFIG = data_config.get_options_container(data_context=ATHENA_DATA_CONTEXT).config
CORE_MARKET_DATASET = OPTIONS_CONFIG.core_market_dataset
SECMASTER = helpers.get_secmaster()

# TODO: leave here for now
FAILED_SYMBOLS = {}
EMPTY_SYMBOLS = {}


def get_outright_1m_price_data(
    trade_date: calendarlib.TradeDate,
    key: symlib.OptionSpecificInstrumentKey,
    version: str,
):
    start_date = trade_date
    if key.exchange != symlib.Exchanges.CBOE:
        # for CME and it's children e.g. CBOT, the start of the trade session
        # starts on the previous day (4pm CT or 10pm UTC)
        # each book file contains data for the whole trade session, however,
        # for each day in the date range given for the data you want
        # from Athena, it will look in that day's dated file for the data
        # so loading in the start of the session requires giving the day before
        # as the start date
        #
        # we are currently starting at 12am for now
        start_date = calendarlib.combine(
            trade_date, calendarlib.FIRST_POSSIBLE_TIME, tzinfo=pytz.UTC
        )
        # start_date = calendarlib.combine(
        #    trade_date.date() - datetime.timedelta(days=1), datetime.time(22, 0, 0, 0), tzinfo=pytz.UTC
        # )
    # print(core_market_dataset.get_outright_1m_price_data(key, args.version).cache_protocol)
    return CORE_MARKET_DATASET.get_outright_1m_price_data(key, version).get_chunk(
        start_date, trade_date
    )


def load_option(client, table, trade_date, opt, mft_symbol, version, retry=5):
    symbol_ids = f"{opt.exchange_symbol}/{opt.exchange_security_id} - {mft_symbol}"
    print(f"Processing {symbol_ids}...")
    # print(f"Instrument key: {opt.instrument_key}")

    if retry == 0:
        print(
            f"Failed to get data for {symbol_ids} on {trade_date}, max retries for reading FSX reached"
        )
        FAILED_SYMBOLS[str(trade_date)].append(f"{symbol_ids}")
        return False

    try:
        data = get_outright_1m_price_data(trade_date, opt.instrument_key, version)
    except OSError as error:
        print(f"Reading from FSX failed. ERROR: {error}. Retrying...")
        return load_option(client, table, trade_date, opt, mft_symbol, version, retry - 1)
    except Exception as error:
        print(f"Failed to get data for {symbol_ids} on {trade_date}. ERROR: {error}")
        FAILED_SYMBOLS[str(trade_date)].append(f"{symbol_ids}")
        return False

    data_to_insert = helpers.get_data_for_research(
        data, opt.exchange, mft_symbol, opt.exchange_symbol, opt.exchange_security_id
    )
    if data_to_insert is None or data_to_insert.empty:
        print(f"Empty data for {symbol_ids} on {trade_date}")
        EMPTY_SYMBOLS[str(trade_date)].append(f"{symbol_ids}")
        return False

    client.write_frame(table, data_to_insert)
    return True


def process_date(
    client, table, gsym, trade_date, version, state, loaded_symbols, skip_loaded_symbol
):
    if str(trade_date) in state["complete"]:
        print(f"Skipping {trade_date}, already in complete list")
        return True

    st = time.time()
    print(f"Processing {trade_date}...")
    success = True

    option_chain = helpers.get_option_chain(gsym, trade_date, SECMASTER)
    print(f"Retrieved {len(option_chain)} options")
    exchange = helpers.get_mapping_from_gsym(gsym).exchange
    for opt in option_chain:
        if gsym == "OSPXW":
            mft_symbol = helpers.get_spxw_mft_symbol(opt)
        elif gsym == "OES":
            mft_symbol = helpers.get_oes_mft_symbol(opt)
        elif symlib.Exchanges.CME in [exchange, exchange.parent]:
            mft_symbol = opt.exchange_symbol
        else:
            print(f"Please implement mft symbol logic for {gsym}")
            sys.exit(0)

        if skip_loaded_symbol and mft_symbol in loaded_symbols:
            print(
                f"Skipping symbol {opt.exchange_symbol}/{opt.exchange_security_id} - {mft_symbol} as it is already in Clickhouse"
            )
            continue
        # if the data couldn't/failed to be read or the data is empty, mark day as failed
        if not load_option(client, table, trade_date, opt, mft_symbol, version):
            success = False
            continue

    et = time.time()
    elapsed_time = et - st
    print("Execution time: ", elapsed_time, " seconds")
    return success


# Helpers for the state file to only use sets
class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        return list(obj)


def as_set(dct):
    newD = {k: set(v) for k, v in dct.items()}
    return newD


def load_data_into_clickhouse(client, table, calendar, args):
    # Load current loaded state
    state = {"complete": set(), "failed": set()}
    state_file_path = args.gsym + "_loading_state.json"
    if os.path.exists(state_file_path):
        with open(state_file_path, "r") as json_file:
            state = json.load(json_file, object_hook=as_set)

    loaded_symbols = helpers.get_all_symbols_from_clickhouse(client, table)
    for trade_date in calendar.get_date_range(args.start, args.end, isoweekdays={1, 2, 3, 4, 5}):
        FAILED_SYMBOLS[str(trade_date)] = []
        EMPTY_SYMBOLS[str(trade_date)] = []
        process = True
        while process:
            try:
                res = process_date(
                    client,
                    table,
                    args.gsym,
                    trade_date,
                    args.version,
                    state,
                    loaded_symbols,
                    args.skip_loaded_symbol,
                )
                if res:
                    print(f"Date {trade_date} loaded")
                    state["complete"].add(str(trade_date))
                    if str(trade_date) in state["failed"]:
                        state["failed"].remove(str(trade_date))
                else:
                    print(f"Date {trade_date} failed to load")
                    if str(trade_date) not in state["failed"]:
                        state["failed"].add(str(trade_date))

                # shutil.rmtree("/tmp/user-data", ignore_errors=True)
                process = False
            except Exception as error:
                print(f"Failed to process for {trade_date}. ERROR: {error}")
                print(traceback.format_exc())

                if str(trade_date) not in state["failed"]:
                    state["failed"].add(str(trade_date))

                process = False

    script_path = os.path.abspath(__name__)
    script_dir = os.path.dirname(script_path)
    results_dir = pathlib.Path(f"{script_dir}/results")
    results_dir.mkdir(parents=True, exist_ok=True)
    ts = time.time_ns()

    with open(state_file_path, "w") as json_file:
        json.dump(state, json_file, cls=JsonEncoder)

    with open(f"{results_dir}/{args.gsym}_failed_symbols_{ts}", "w") as f:
        json.dump(FAILED_SYMBOLS, f)

    with open(f"{results_dir}/{args.gsym}_empty_symbols_{ts}", "w") as f:
        json.dump(EMPTY_SYMBOLS, f)


def get_args():
    parser = argparse.ArgumentParser(description="Script to load 1m option data into Clickhouse")
    parser.add_argument("--reset-clickhouse", action="store_true")
    parser.add_argument("--create-clickhouse", action="store_true")
    parser.add_argument("--skip-loaded-symbol", action="store_true")
    parser.add_argument("--start", type=int, help="date to start loading like 20230914")
    parser.add_argument("--end", type=int, help="date to end loading like 20230914")
    parser.add_argument("--gsym", type=str, help="group to load like OSPXW, OES")
    parser.add_argument("--version", type=str, help="version of the data")
    return parser.parse_args()


def main():
    args = get_args()

    if not args.start:
        print("You need to provide a start date")
        sys.exit(1)
    if not args.end:
        args.end = args.start
    if args.end < args.start:
        print("Provide an end date >= start date")
        sys.exit(1)

    if args.gsym == "OSPXW":
        table_name = "historical.spxw_1min_v1"
        calendar = OPTIONS_CONFIG.ref_data_config.cboe_calendar
    elif args.gsym == "OES":
        table_name = "historical.oes_1min_pcap_v2"
        calendar = OPTIONS_CONFIG.ref_data_config.cme_calendar
    elif args.gsym in ["OS0", "OTY", "OCL"]:
        table_name = f"historical.{args.gsym.lower()}_1min_pcap_v1"
        calendar = OPTIONS_CONFIG.ref_data_config.cme_calendar

    client = clickhouse.get_write_client(table_name, location="sy5")
    table = clickhouse.get_write_table(table_name, location="sy5")
    if args.reset_clickhouse or args.create_clickhouse:
        print(
            "Resetting table clickhouse..."
            if args.reset_clickhouse
            else "Creating table in clickhouse..."
        )
        helpers.create_click_house_table(client, table, drop_table=args.reset_clickhouse)

    load_data_into_clickhouse(client, table, calendar, args)

    client.execute(f"OPTIMIZE TABLE {table} FINAL")
    print("End of execution")


if __name__ == "__main__":
    main()
