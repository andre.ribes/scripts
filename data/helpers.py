from __future__ import annotations

import calendar
import datetime
import re
import sys

import numpy
import pandas
from epoch_data import calendarlib, hdstorelib
from epoch_markets import symlib
from epoch_markets.mdcatalog.options import job
from epoch_markets.symlib import options


def get_secmaster(path_prefix: str = "/fsxcache/output") -> symlib.SecMaster:
    data_client = hdstorelib.MainDataClient(
        protocol="file",
        host=None,
        name_resolver=None,
        network_latency=None,
        path_prefix=path_prefix,
    )

    fronts_factory = symlib.FrontsFactory(
        data_client=data_client,
        base_path="secdef",
        front_scheme="mft_rel",
        ignore_missing_products=True,
    )

    secmaster = symlib.SecMaster(
        data_client=data_client,
        base_path="secdef",
        fronts_factory=fronts_factory,
    )

    return secmaster


def get_mapping_from_gsym(gsym: str) -> symlib.InstrumentMapping:
    return symlib.InstrumentKey.get_instrument_mapping(gsym)


# get all symbols for some trade date
def get_exchange_symbols(
    gsym: str, trade_date: datetime.date, is_complex: bool, secmaster
) -> list[str]:
    return job.get_exchange_symbols(gsym, trade_date, is_complex, secmaster)


def get_exchange_security_ids(
    gsym: str, trade_date: datetime.date, is_complex: bool, secmaster
) -> list[int]:
    return job.get_exchange_security_ids(gsym, trade_date, is_complex, secmaster)


def create_option_specific_key(
    gsym: str, expiry_date: datetime.date, strike: float, pc_flag: str
) -> options.OptionSpecificInstrumentKey:
    o_key = symlib.OptionInstrumentKey(gsym, expiry_date)
    o_specific = symlib.OptionSpecificInstrumentKey(
        o_key,
        expiration_date=calendarlib.TradeDate(expiry_date.year, expiry_date.month, expiry_date.day),
        strike_price=strike,
        put_or_call=options.PutOrCall.from_str_value(pc_flag),
    )
    return o_specific


def create_option_specific_key_using_exchange_symbol(
    gsym: str, expiry_date: datetime.date, exchange_symbol: str
) -> options.OptionSpecificInstrumentKey:
    o_key = symlib.OptionInstrumentKey(gsym, expiry_date)
    o_specific = symlib.OptionSpecificInstrumentKey(o_key, exchange_symbol=exchange_symbol)
    return o_specific


def create_option_specific_key_using_exchange_security_id(
    gsym: str, expiry_date: datetime.date, exchange_security_id: int
) -> options.OptionSpecificInstrumentKey:
    o_key = symlib.OptionInstrumentKey(gsym, expiry_date)
    o_specific = symlib.OptionSpecificInstrumentKey(
        o_key, exchange_security_id=exchange_security_id
    )
    return o_specific


# gsyms: OSPX, OSPXW, OES
def get_option_chain(
    gsym: str, trade_date: calendarlib.DateLike, secmaster
) -> list[options.OptionInstrument]:
    o_key = options.OptionInstrumentKey(gsym, trade_date)
    return secmaster.get_option_chain(o_key, last_eligible_trade_date=o_key.date)


class OptionConstants:
    CallMonths = {
        "A": 1,
        "B": 2,
        "C": 3,
        "D": 4,
        "E": 5,
        "F": 6,
        "G": 7,
        "H": 8,
        "I": 9,
        "J": 10,
        "K": 11,
        "L": 12,
    }
    PutMonths = {
        "M": 1,
        "N": 2,
        "O": 3,
        "P": 4,
        "Q": 5,
        "R": 6,
        "S": 7,
        "T": 8,
        "U": 9,
        "V": 10,
        "W": 11,
        "X": 12,
    }
    CallMonthCodes = {month: code for code, month in CallMonths.items()}
    PutMonthCodes = {month: code for code, month in PutMonths.items()}


def nth_weekday_inv_n(d: datetime.date) -> int | None:
    """
    A date has a weekday (say Friday). It will be the N Friday of the month.
    The function returns N (1-5).

    For instance if the date is the 3rd Monday of the month, it will return 3.
    """
    cal = calendar.Calendar()  # Week starts Monday
    weeks = cal.monthdayscalendar(d.year, d.month)
    for x in range(len(weeks)):
        if d.day in weeks[x]:
            week_number = x
            if d.day - week_number * 7 > 0:
                return week_number + 1
            else:
                return week_number
    return None


def get_spxw_mft_symbol(opt: options.OptionInstrument):
    # Put = 0; Call = 1
    strike_price = int(opt.strike_price)
    month_code = (
        OptionConstants.CallMonthCodes[opt.expiration_date.month]
        if opt.put_or_call == 1
        else OptionConstants.PutMonthCodes[opt.expiration_date.month]
    )
    end = "00.U"
    if strike_price >= 1000:
        month_code = month_code.lower()
        end = "0.U"
    mft_symbol = f'SPXW{month_code}{opt.expiration_date.strftime("%d%y")}{strike_price}{end}'
    return mft_symbol


def get_oes_mft_symbol_stem_name(opt: options.OptionInstrument):
    # Quaterly option
    if opt.product_symbol == "ES":
        return "ES"
    # Monthly option
    if opt.product_symbol == "EW":
        return "EW"
    # Monday
    m = re.match(r"E(?P<week>[1-5])A", opt.product_symbol)
    if m is not None:
        return f"1AE{m.group('week')}W"
    # Tuesday
    m = re.match(r"E(?P<week>[1-5])B", opt.product_symbol)
    if m is not None:
        return f"E1B{m.group('week')}W"
    # Wednesday
    m = re.match(r"E(?P<week>[1-5])C", opt.product_symbol)
    if m is not None:
        return f"EC{m.group('week')}W"
    # Thursday
    m = re.match(r"E(?P<week>[1-5])D", opt.product_symbol)
    if m is not None:
        return f"E1D{m.group('week')}W"
    # Friday
    m = re.match(r"EW(?P<week>[1-5])", opt.product_symbol)
    if m is not None:
        return f"ES{m.group('week')}W"
    print(f"Could not find OES stem name for {opt.exchange_symbol}/{opt.product_symbol}")
    sys.exit(1)


def get_oes_mft_symbol(opt: options.OptionInstrument):
    stem_name = get_oes_mft_symbol_stem_name(opt)
    strike_code = int(opt.strike_price)
    month_code = (
        OptionConstants.CallMonthCodes[opt.expiration_date.month]
        if opt.put_or_call == 1
        else OptionConstants.PutMonthCodes[opt.expiration_date.month]
    )
    year_code = opt.expiration_date.strftime("%y")
    return f"{stem_name}{strike_code}{month_code}{year_code}"


def create_click_house_table(click_client, click_table, drop_table=False):
    if drop_table:
        click_client.execute(f"DROP TABLE IF EXISTS {click_table}")

    table_schema = f"""
        CREATE TABLE IF NOT EXISTS {click_table}
        (`timestamp` DateTime64(3),
         `symbol` LowCardinality(String),
         `mft_symbol` LowCardinality(String),
         `exchange_symbol` LowCardinality(String),
         `exchange_security_id` Int32,
         `bid_size` Float64,
         `bid` Float64,
         `ask` Float64,
         `ask_size` Float64,
         `mid` Float64,
         `spread` Float64,
         `volume` Float64,

         PROJECTION symbol_agg
         (
         SELECT
            mft_symbol,
            count(*),
            min(timestamp),
            max(timestamp)
         GROUP BY mft_symbol
         ),

         PROJECTION date_symbol_agg
         (
         SELECT
            toDate(timestamp),
            mft_symbol,
            count(*)
         GROUP BY
            toDate(timestamp),
            mft_symbol
         )
        )
        ENGINE = ReplacingMergeTree
        PRIMARY KEY (mft_symbol, timestamp)
        ORDER BY (mft_symbol, timestamp)
        SETTINGS index_granularity = 8192;
    """
    click_client.execute(table_schema)


def get_data_for_research(
    df: pandas.DataFrame,
    exchange: symlib.Exchange,
    mft_symbol: str,
    exchange_symbol: str,
    exchange_security_id: int,
):
    display_factor = 1 / 1e11 if symlib.Exchanges.CME in [exchange, exchange.parent] else 1 / 1e9
    df["bid"] = df.apply(
        lambda row: row["bid"] * display_factor if row["bid"] != 0.0 else numpy.nan, axis=1
    )
    df["ask"] = df.apply(
        lambda row: row["offer"] * display_factor if row["offer"] != 0.0 else numpy.nan, axis=1
    )
    df["mid"] = df.apply(
        lambda row: (row["ask"] + row["bid"]) / 2.0 if row["bid"] != 0.0 else numpy.nan, axis=1
    )
    df["spread"] = df["abs_spread"] * display_factor
    df["symbol"] = mft_symbol
    df["mft_symbol"] = mft_symbol
    df["exchange_symbol"] = exchange_symbol
    df["exchange_security_id"] = exchange_security_id
    df["timestamp"] = df.index
    df["volume"] = numpy.nan
    df.rename(columns={"offer_size": "ask_size"}, inplace=True)
    return df[
        [
            "timestamp",
            "bid",
            "ask",
            "bid_size",
            "ask_size",
            "mid",
            "spread",
            "symbol",
            "mft_symbol",
            "exchange_symbol",
            "exchange_security_id",
            "volume",
        ]
    ]


def get_all_symbols_from_clickhouse(click_client, click_table):
    query = f"""
        SELECT DISTINCT mft_symbol FROM {click_table}
    """
    symbols = click_client.execute(query)

    return [sym[0] for sym in symbols]
