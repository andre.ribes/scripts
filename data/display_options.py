import argparse

import helpers
from epoch_infra import commonlib
from epoch_markets.symlib import options

commonlib.init_logging("DEBUG")
secmaster = helpers.get_secmaster()

parser = argparse.ArgumentParser(description="Script to show an option chain")
parser.add_argument("--trade-date", type=int, help="last trade date of options")
parser.add_argument("--gsym", type=str, help="group to load like OSPXW, OES")
args = parser.parse_args()


# OptionInstrument(
# instrument_key=OptionSpecificInstrumentKey={option_instr_key: OptionInstrumentKey={internal_product_code: OES, date: 20230731}, expiration_date: None, strike_price: None, put_or_call: None, exchange_symbol: EWN3 P4760, exchange_security_id: 4128015},
# exchange=Exchange(name='CME'), exchange_symbol='EWN3 P4760', exchange_security_id=4128015, group_symbol='EW', activation_date=datetime.datetime(2023, 1, 27, 21, 25), expiration_date=datetime.datetime(2023, 7, 31, 20, 0), display_factor=0.01, currency=<Currency.USD: 0>, strike_price=4760.000000000001, put_or_call=<PutOrCall.Put: 0>, tick_size=None, point_value=None, term=None, channel='311')
def __str__(self):
    return f"sym={self.exchange_symbol}, sec_id={self.exchange_security_id}, pd_symbol={self.product_symbol}, gp_symbol={self.group_symbol} exp_date={self.expiration_date}, strk_pc={self.strike_price}, pc={self.put_or_call}"


options.OptionInstrument.__str__ = __str__

option_chain = helpers.get_option_chain(args.gsym, args.trade_date, secmaster)
for opt in option_chain:
    print(opt)
