import socket
import struct
import sys
import capnp

# to find the capnp file
sys.path.append('.')
import volatility_capnp

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
# server_address = ('ny5-mft-tbricks01', 10000)
server_address = ('ny5-mft-tbricks03', 15000)


print('connecting to {} port {}'.format(*server_address))
sock.connect(server_address)
print('connected')

def recv_size(the_socket, size_to_receive):

    #print ("Want to receive {}".format(size_to_receive))
    size_remaining = size_to_receive
    data = b''
    while True:
        sock_data = the_socket.recv(size_remaining)
        data += sock_data
        if len(sock_data) < size_remaining:
            size_remaining -= len(sock_data)
        else:
            return data

try:
    while True:
        
        # First always decode the size of the message with the prefix
        # prefix = 8 bytes (4 bytes = count of segments + N x 4 bytes = sizes of the segments)
        expected_seg = recv_size(sock, 4) #sock.recv(4)
        #expected_seg = sock.recv(4)
        num_of_seg = int.from_bytes(expected_seg, byteorder='little', signed="False")
        #print("Number of segments: {}".format(num_of_seg + 1))
        expected_size_of_segments = recv_size(sock, 4)#sock.recv(4)
        #print (expected_size_of_segments)
        seg_size = int.from_bytes(expected_size_of_segments, byteorder='little', signed="False")
        #print("size of segments: {}".format(seg_size))
        # The size is in multiplier of 8 bytes
        seg_size_in_bytes = seg_size * 8
        data = recv_size(sock, seg_size_in_bytes)#sock.recv(seg_size_in_bytes)

        #print (expected_seg)
        #print (expected_size_of_segments)
        #print (data)

        full_message = expected_seg + expected_size_of_segments + data
        #print (full_message)

        with volatility_capnp.InstrumentGreeks.from_bytes(full_message) as vol:
            print("{} - {} Strike:{} Delta:{} Gamma:{} CalendarTheta:{} Vega:{}".format(vol.time, vol.symbol, vol.strikePrice, vol.delta, vol.gamma, vol.calendarTheta, vol.vega))

finally:
    print('closing socket')
    sock.close()
