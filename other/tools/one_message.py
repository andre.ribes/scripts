import socket
import sys
import json
import capnp

# to find the capnp file
sys.path.append('.')
import tbricks_capnp


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
# server_address = ('ny5-mft-tbricks01', 10000)
server_address = ('ny5-mft-tbricks03', 10001)


print('connecting to {} port {}'.format(*server_address))
sock.connect(server_address)


mode = 'text'

if len(sys.argv) == 2:
    if sys.argv[1] == '-json':
        mode = 'json'
    elif sys.argv[1] == '-capnp':
        mode = 'capnp'


print('mode selected is {}'.format(mode))



try:

    if mode == 'text':
        # Send data
        message = b'Sending a text message'
        print('sending {!r}'.format(message))
        sock.sendall(message)

    elif mode == 'json':
        message = {'Information': 'This is a JSON message'}
        print('sending {!r}'.format(message))
        sock.sendall(json.dumps(message).encode())

    elif mode == 'capnp':
        info = tbricks_capnp.Information.new_message()
        info.message = 'My first binary message using capnp'
        sock.sendall(info.to_bytes())

    # Look for the response
    data = sock.recv(1024)
    print('received {!r}'.format(data))
    test = tbricks_capnp.Data.from_bytes_packed(data)
    print('instrument ' + test.instrument)

    

finally:
    print('closing socket')
    sock.close()
