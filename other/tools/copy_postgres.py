from octopus import postgres

source = 'ny5-mft-ropt02:5432'
target = 'ny5-mft-ropt02:5432'
#feed = 'BBG'
feed = 'RFA'
#target = 'ny5-mft-opt03:5432'

source_env = 'relopt01'
dest_env = 'testopt01'

sql = f'''
SELECT * FROM market_Data
WHERE env = '{source_env}'
AND source = '{feed}'
AND timestamp >= '2024-11-21 10:35:00+00:00'
AND timestamp < '2024-11-21 10:36:00+00:00'
ORDER BY timestamp ASC
'''

drop_first_row = True


with postgres.PostgresClient(source, database='trader') as client:
    df = client.read_frame(sql)

df['env'] = dest_env
if drop_first_row:
    df = df.iloc[1:]

print (f'Size to process: {len(df.index)}')

start = 0
end = 0
for i in range(0, len(df.index), 100000):
    start = end
    if i == 0:
        continue
    end = i
    print (f"Processing {start}, {end}")
    df_temp = df.iloc[start:end]
    with postgres.PostgresClient(target, database='trader') as client:
       client.write_frame(table='market_data', data=df_temp)
start = end
print (f"Processing {start}, {end}")
df_temp = df.iloc[start:]
with postgres.PostgresClient(target, database='trader') as client:
   client.write_frame(table='market_data', data=df_temp)
