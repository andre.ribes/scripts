from __future__ import annotations

import argparse
import datetime
import logging
import pandas
from octopus import postgres
from typing import Any

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)


def parse_cmdline() -> Any:
    parser = argparse.ArgumentParser(
        description="Fake trader to experiment with the Kafka client lib"
    )
    parser.add_argument(
        "--env",
    )
    parser.add_argument(
        "--feed",
    )
    parser.add_argument(
        '--ref_ts',
    )
    parser.add_argument(
        '--from_ts',
    )
    parser.add_argument(
        '--to_ts',
    )
    parser.add_argument(
        '--table',
        default='market_data',
    )
    parser.add_argument(
        '--src_db',
        default='ny5-mft-ropt02:5432',
    )
    parser.add_argument(
        '--target_db',
        default='ny5-mft-ropt02:5432',
    )
    parser.add_argument(
        '--dry_run',
        action='store_true',
        default=False,
    )
    return parser.parse_args()


args = parse_cmdline()

logging.info(args)
source = args.src_db
target = args.target_db

# Step 1 - Get Reference Data
ref_data_sql = f'''
SELECT * FROM {args.table}
WHERE env = '{args.env}'
AND source = '{args.feed}'
AND timestamp = '{args.ref_ts}'
ORDER BY timestamp ASC
'''

with postgres.PostgresClient(source, database='trader') as client:
    logging.info(f'source data query {ref_data_sql}')
    df = client.read_frame(ref_data_sql)
logging.info(f'Source Data: {df}')

start_ts = pandas.Timestamp(args.from_ts)
end_ts = pandas.Timestamp(args.to_ts)

while start_ts <= end_ts:
    logging.info(f'Processing timestamp: {start_ts}')
    df.timestamp = start_ts
    logging.info(f'Size to process: {len(df.index)}')
    start = 0
    end = 0
    for i in range(0, len(df.index), 100000):
        start = end
        if i == 0:
            continue
        end = i
        logging.info(f"Processing batch {start}, {end}")
        df_temp = df.iloc[start:end]
        with postgres.PostgresClient(target, database='trader') as client:
            client.write_frame(table=f'{args.table}', data=df_temp, dry_run=args.dry_run)
    start = end
    logging.info(f"Processing batch {start}, {end}")
    df_temp = df.iloc[start:]
    with postgres.PostgresClient(target, database='trader') as client:
        client.write_frame(table=f'{args.table}', data=df_temp, dry_run=args.dry_run)
    # Increment by 1 minute
    start_ts += datetime.timedelta(minutes=1)
