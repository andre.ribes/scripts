#!/bin/bash
echo "----------------------------"
echo "Local Time"
date
echo "----------------------------"
#echo ""
#echo "Time in Los Angeles"
#export TZ=America/Los_Angeles
#date
#echo ""
#echo "Time in San Francisco"
#export TZ=US/Pacific
#date
echo ""
echo "Time in Chicago,"
export TZ=US/Central
date
echo ""
echo "Time in New York"
export TZ=America/New_York
date
echo ""
echo "^"
echo "|"
echo "|"
echo "----------------------------"
echo "Time in UTC"
export TZ=UTC
date
echo "----------------------------"
echo "|"
echo "|"
echo "v"
echo ""
echo "Time in Hong_Kong"
export TZ=Asia/Hong_Kong
date
echo ""
echo "Time in Tokyo"
export TZ=Asia/Tokyo
date
echo ""
echo "Time in Sydney"
export TZ=Australia/Sydney
date
echo ""
unset TZ
