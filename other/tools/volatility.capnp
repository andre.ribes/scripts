@0xb4e7b03f546024e6;

struct InstrumentGreeks {
    time      @0 :Text;
    timestamp @1 :UInt64;

    # Influx Tags
    env          @2 :Text;
    shortName    @3 :Text; 
    longName     @4 :Text; 
    symbol       @5 :Text;
    putCallFlag  @6 :Text;
    strikePrice  @7 :Float64; # Note is also going to be used as a field named "Strike"
    maturityDate @8 :Text;

    # Greeks
    forwardPrice         @9  :Float64;
    volatility           @10 :Float64;
    delta                @11 :Float64;
    volatilityTheta      @12 :Float64;
    gamma                @13 :Float64;
    vega                 @14 :Float64;
    rho                  @15 :Float64;
    daysToMaturity       @16 :Float64;
    fairAskPrice         @17 :Float64;
    fairBidPrice         @18 :Float64;
    fairMarketPrice      @19 :Float64;
    financingRate        @20 :Float64;
    calendarTheta        @21 :Float64;
    skewDelta            @22 :Float64;
    skewGamma            @23 :Float64;
    volatilityTime       @24 :Float64;
    askImpliedVolatility @25 :Float64;
    bidImpliedVolatility @26 :Float64;
    impliedVolatility    @27 :Float64;
    askUsed              @28 :Float64;
    bidUsed              @29 :Float64;
    expirationTime       @30 :Float64;
}