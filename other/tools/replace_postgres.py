import pandas
from octopus import postgres

sql = '''
SELECT * FROM market_Data
WHERE env = 'testopt01'
AND source = 'BBG'
AND timestamp = '2024-08-30 01:38:00'
ORDER BY timestamp ASC
'''

source = 'ny5-mft-ropt02:5432'
target = 'ny5-mft-ropt02:5432'

with postgres.PostgresClient(source, database='trader') as client:
    df = client.read_frame(sql)

df.timestamp = pandas.to_datetime('2024-08-30 01:37:00')

print (f'Size to process: {len(df.index)}')
start = 0
end = 0
for i in range(0, len(df.index), 100000):
    start = end
    if i == 0:
        continue
    end = i
    print (f"Processing {start}, {end}")
    df_temp = df.iloc[start:end]
    with postgres.PostgresClient(target, database='trader') as client:
       client.write_frame(table='market_data', data=df_temp)
start = end
print (f"Processing {start}, {end}")
df_temp = df.iloc[start:]
with postgres.PostgresClient(target, database='trader') as client:
   client.write_frame(table='market_data', data=df_temp)
