import datetime
from octopus import assets, universe


# print ('Last Week of September')
# d = datetime.date(2023,9,29)
# spxw_last_week_sep = universe.get_universe_symbols('zeno_universe_weekly', 'OSPXW', date=d)
# for x in spxw_last_week_sep:
#     assets.get(x).bbg()

d = datetime.date(2023,11,27)
print (f'Symbols for week starting on {d}')
symbols = universe.get_universe_symbols('zeno_universe_weekly', 'OESW', date=d)
print (f'Number of synbols in OESW: {len(symbols)}')
for x in symbols:
    print (assets.get(x).get_bbg_symbol())

symbols = universe.get_universe_symbols('zeno_universe_weekly', 'OSPXW', date=d)
print (f'Number of synbols in OSPXW: {len(symbols)}')
for x in symbols:
    print (assets.get(x).get_bbg_symbol())