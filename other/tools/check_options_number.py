#!/usr/bin/env python

import os
import datetime
import logging
import numpy
import pandas
import pathlib
import requests

from core import refdata as core_refdata
from octopus import clickhouse as cl
from octopus import dotmft
from octopus import datelib
from octopus import loghelp
from octopus import options_utils
from octopus import refdata_options
from octopus import secmaster_options_refdata as sec_opt_ref

# python check_options_number.py --start_date 20240823 --end_date 20240830 --cme_option_base_assets OCLW ONQW --expiry_dates 20240826 20240827 20240828 20240829 20240830

def parse_cmdline():
    import argparse

    parser = argparse.ArgumentParser(description='Check available options per day')
    parser.add_argument('--start_date', type=datelib.coerce_date, required=True)
    parser.add_argument('--end_date', type=datelib.coerce_date, required=True)
    parser.add_argument(
        '--refdata_server',
        default=core_refdata.get_root_url(),
        help='Location of the Epoch Refdata service files',
    )
    parser.add_argument(
        '--refdata_uat_server',
        default=core_refdata.get_root_uat_url(),
        help='Location of the Epoch Refdata uat service files',
    )
    parser.add_argument(
        '--use_uat_refdata_server',
        action='store_true',
        help='Use uat refdata server address instead of prod one',
    )
    parser.add_argument(
        '--cme_option_base_assets',
        nargs='+',
        required=True
    )
    parser.add_argument(
        '--expiry_dates',
        nargs='+',
        type=datelib.coerce_date,
        required=True
    )
    return parser.parse_args()


def main():
    args = parse_cmdline()

    if args.use_uat_refdata_server:
        logging.info('uat refdata server selected')
        url= args.refdata_uat_server
    else:
        logging.info('live refdata server selected')
        url= args.refdata_server

    # Step 1: Load all assets from cme
    refdata_json = {}

    days = int((args.end_date - args.start_date).days) + 1
    for date in (args.start_date + datetime.timedelta(days=n) for n in range(days)):

        domain = 'cme'
        domain_url = url + domain + '/' + date.strftime('%Y/%m/%d') + '/latest'
        logging.info(f'Loading from {domain_url}')
        try:
            resp = requests.get(domain_url)
            refdata_json[(domain, date)] = pandas.DataFrame(resp.json())
        except:  # noqa: E722
            logging.info(f'refdata for {date} is not available')

    for cme_base_asset in args.cme_option_base_assets:
        logging.info(f'Processing asset {cme_base_asset}')
        opt_family = sec_opt_ref.OPTION_FAMILY_BY_BASE_ASSET[cme_base_asset]
        results = []
        for expiry_date in args.expiry_dates:
            for domain, date in refdata_json:
                df = refdata_json[(domain, date)]
                df.globexLastTradeDate = pandas.to_datetime(df.globexLastTradeDate)
                df = df[df.globexLastTradeDate.dt.date == expiry_date]
                results.append({
                    'date': date,
                    'expiry_date': expiry_date, 
                    'options': len(df[df.globexProductCode.isin(opt_family.product_codes())].index),
                    'asset': cme_base_asset,
                })

                previous_date = date + datetime.timedelta(days=-1)
                if (domain, previous_date) in refdata_json:
                    df_previous = refdata_json[(domain, previous_date)]
                    df_previous.globexLastTradeDate = pandas.to_datetime(df_previous.globexLastTradeDate)
                    df_previous = df_previous[df_previous.globexLastTradeDate.dt.date == expiry_date]

                    print (f'Comparing {date} and {previous_date}')
                    print (numpy.setdiff1d(df['epochSymbol'], df_previous['epochSymbol']))

        print (pandas.DataFrame.from_records(results))

if __name__ == '__main__':
    loghelp.init_batch_logging('INFO')

    main()