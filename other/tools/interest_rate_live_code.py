# Live calculation of box spread strategy to compute interest rate
# Currently not getting the right results
def calc_interest_rate(data: pandas.DataFrame, futures, future, expiry, best_result) -> float:
    # Box spread strategy
    # TODO - Safety Checks
    # TODO - Store results to converge during the day
    # TODO - for weeklies and monthlies on the same expiry compute two interest rates

    e_data = data[(data.expirydate == expiry) & (data.future == future)]
    strikes = sorted(e_data.strike.dropna().unique())
    implied_bid_IR = -1.e09
    implied_ask_IR = 1.e09
    processed_at_least_one_box = False
    results = {}

    if best_result != {}:
        box_value_s = best_result['BoxValue']
        bid_cost_s = best_result['BidCost']
        ask_cost_s = best_result['AskCost']
        K1_call_s = best_result['K1_call']
        K1_put_s  = best_result['K1_put']
        K2_call_s = best_result['K2_call']
        K2_put_s  = best_result['K2_put']
        implied_bid_IR = best_result['BidIR']
        implied_ask_IR = best_result['AskIR']

        spread = implied_ask_IR - implied_bid_IR
        results[abs(spread)] = {
            'BoxValue': box_value_s,
            'BidCost': bid_cost_s,
            'AskCost': ask_cost_s,
            'K1_call': K1_call_s,
            'K1_put': K1_put_s,
            'K2_call': K2_call_s,
            'K2_put': K2_put_s,
            'BidIR': implied_bid_IR,
            'AskIR': implied_ask_IR,
            'IR': (implied_bid_IR + implied_ask_IR) / 2.0
        }

    dte = (e_data.iloc[0].asset.last_trade_date - timelib.now().tz_convert(e_data.iloc[0].asset.timezone).date()).days
    e_data_future = futures[future]
    base_price = (e_data_future['bid'] + e_data_future['ask']) / 2.0
    logging.info(f'Number of strikes:{len(strikes)}')

    for low_strike in strikes[:-1]:
        if low_strike >= base_price:
            continue
        for high_strike in strikes[1:]:
            if high_strike < base_price:
                continue

            processed_at_least_one_box = True 
            K1_call = e_data[(e_data.put_call_flag == 'C') & (e_data.strike == low_strike)]
            K1_put  = e_data[(e_data.put_call_flag == 'P') & (e_data.strike == low_strike)]
            K2_call = e_data[(e_data.put_call_flag == 'C') & (e_data.strike == high_strike)]
            K2_put  = e_data[(e_data.put_call_flag == 'P') & (e_data.strike == high_strike)]

            if len(K1_call.index) == 0 or len(K1_put.index) == 0 or len(K2_call.index) == 0 or len(K2_put.index) == 0:
                # TODO to move to debug
                logging.info(f'Cannot compute box for the following strikes {low_strike}:{high_strike}')
                logging.info(f'Instruments found {low_strike}:Call({len(K1_call.index)})/Put({len(K1_put.index)})')
                logging.info(f'Instruments found {high_strike}:Call({len(K2_call.index)})/Put({len(K2_put.index)})')
                continue

            K1_call = K1_call.iloc[0].T.squeeze()
            K1_put  = K1_put.iloc[0].T.squeeze()
            K2_call = K2_call.iloc[0].T.squeeze()
            K2_put  = K2_put.iloc[0].T.squeeze()
            bid_cost = K1_call.bid - K2_call.ask  + K2_put.bid - K1_put.ask
            ask_cost = K1_call.ask - K2_call.bid  + K2_put.ask - K1_put.bid
            box_value = (high_strike - low_strike)
            ask_IR = (box_value - bid_cost) / bid_cost * 365.0 / dte
            bid_IR = (box_value - ask_cost) / ask_cost * 365.0 / dte
            spread = ask_IR - bid_IR
            if abs(spread) < abs(implied_ask_IR - implied_bid_IR):
                box_value_s = box_value 
                bid_cost_s = bid_cost
                ask_cost_s = ask_cost
                K1_call_s = K1_call
                K1_put_s  = K1_put
                K2_call_s = K2_call
                K2_put_s  = K2_put
                implied_bid_IR = bid_IR
                implied_ask_IR = ask_IR

                results[abs(spread)] = {
                    'BoxValue': box_value_s,
                    'BidCost': bid_cost_s,
                    'AskCost': ask_cost_s,
                    'K1_call': K1_call,
                    'K1_put': K1_put,
                    'K2_call': K2_call,
                    'K2_put': K2_put,
                    'BidIR': implied_bid_IR,
                    'AskIR': implied_ask_IR,
                    'IR': (implied_bid_IR + implied_ask_IR) / 2.0
                }

    if not processed_at_least_one_box:
        logging.info(f'Future:{future} Expiry:{expiry} Could not find a valid box to compute the interest rate - could not find strikes below/above future')
        return 0.0, {}

    if implied_bid_IR == -1e09:
        logging.info(f'Future:{future} Expiry:{expiry} Could not find a valid box to compute the interest rate')
        return 0.0, {}

    interest_rate = (implied_bid_IR + implied_ask_IR) / 2.0
    logging.info('Result:')
    logging.info(f'{K1_call_s.asset.longname} - {K1_put_s.asset.longname} --- {K2_call_s.asset.longname} - {K2_put_s.asset.longname}')
    logging.info(f'K1Call: {K1_call_s.bid}/{K1_call_s.ask} - K1Put: {K1_put_s.bid}/{K1_put_s.ask} --- K2Call: {K2_call_s.bid}/{K2_call_s.ask} - K2Put: {K2_put_s.bid}/{K2_put_s.ask}')
    logging.info(f'Future:{future} Expiry:{expiry} BasePrice:{base_price} BoxValue:{box_value_s} BidCost:{bid_cost_s} AskCost:{ask_cost_s} DTE:{dte}')
    logging.info(f'IBid:{implied_bid_IR} IAsk:{implied_ask_IR} IR:{interest_rate}')

    # Print the best three
    i = 2
    logging.info('')
    logging.info('Bests:')
    best_result = {}
    for res in sorted(results.keys()):
        if i < 0:
            break
        if i == 2:
            best_result = results[res]
            best_result['abs_spread'] = res
        d = results[res]
        box_value_s = d['BoxValue']
        bid_cost_s = d['BidCost']
        ask_cost_s = d['AskCost']
        K1_call_s = d['K1_call']
        K1_put_s  = d['K1_put']
        K2_call_s = d['K2_call']
        K2_put_s  = d['K2_put']
        implied_bid_IR = d['BidIR']
        implied_ask_IR = d['AskIR']
        logging.info(f'{K1_call_s.asset.longname} - {K1_put_s.asset.longname} --- {K2_call_s.asset.longname} - {K2_put_s.asset.longname}')
        logging.info(f'Spread: {implied_ask_IR - implied_bid_IR}')
        logging.info(f'K1Call: {K1_call_s.bid}/{K1_call_s.ask} - K1Put: {K1_put_s.bid}/{K1_put_s.ask} --- K2Call: {K2_call_s.bid}/{K2_call_s.ask} - K2Put: {K2_put_s.bid}/{K2_put_s.ask}')
        logging.info(f'Future:{future} Expiry:{expiry} BasePrice:{base_price} BoxValue:{box_value_s} BidCost:{bid_cost_s} AskCost:{ask_cost_s}')
        logging.info(f'IBid:{implied_bid_IR} IAsk:{implied_ask_IR} IR:{d["IR"]}')
        i -= 1
    logging.info('')

    return interest_rate, best_result
