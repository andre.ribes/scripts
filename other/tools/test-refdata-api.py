import requests

def get_service_location(consul_url: str, service_name: str) -> str:
    print(f"Fetching consul service location for {service_name}...\n")
    response = requests.get(f"{consul_url}:8500/v1/catalog/service/{service_name}")
    service_data = response.json()
    services = [f"http://{x['ServiceAddress']}:{x['ServicePort']}" for x in service_data]
    if len(services) == 0:
        raise Exception("Consul service definition request yielded no results")
    return services[0]

print(get_service_location('http://10.1.0.18', 'refdata-api'))
