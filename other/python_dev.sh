#!/usr/bin/env bash
# See https://youtrack.jetbrains.com/issue/PY-35978/Support-conda-with-remote-interpreters
set -e

# Bring in mft-dist aliases
# shellcheck disable=SC1091
source "${HOME}/mft-dist/dotfiles/alias"

# Activate pnl env quietly
act dev > /dev/null

# Defaults to unicorn repo, so we cd to the parent
cd /home/andre.ribes

export PYTHONPATH=/home/andre.ribes/var/src/dev:$PYTHONPATH
export PYTHONPATH=/home/andre.ribes/var/src/dev/octopus:$PYTHONPATH
export PYTHONPATH=/home/andre.ribes/.vscode-server/extensions/ms-python.python-2024.16.1-linux-x64/python_files:$PYTHONPATH

# Forward all args to active python
# export ASAN_OPTIONS=verify_asan_link_order=0:verbosity=1
python "$@"
