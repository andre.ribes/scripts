#!/usr/bin/env python
import pandas
from octopus import shtools

start_date = "20160816"
end_date = "20230703"
archive = "1m-ohlc-bid-ask-size-daily-opt"
regex = "EW.*"
database = "historical.trthopt_1min"

def execute(s_date, e_date):
    cmdline = shtools.get_cmdline(
        ('runinenv', 'rss', 'rsstochouse'),
        archive, database,
        start_date=s_date,
        end_date=e_date,
        symbol_regex=regex
    )
    shtools.xargs(
        "", cmdline, 2, False
    )

dates_ = pandas.date_range(start_date, end_date, freq='SM')
s_date = pandas.to_datetime(start_date).date()
dates = [ts.date() for ts in dates_]

for date in dates:
    execute(s_date, date)
    s_date = date + pandas.Timedelta(days=1)

e_date = pandas.to_datetime(end_date).date()
execute(s_date, e_date)