import sys
import pandas
from datetime import timedelta
from mfrealtime import influx

iql = """\
    SELECT *
      FROM {table}
     WHERE time > '{stime:%Y-%m-%dT%H:%M:%SZ}'
       AND time < '{etime:%Y-%m-%dT%H:%M:%SZ}'
       AND env = '{env}'"""

iql_all = """\
    SELECT * FROM {table}
    """

def _load_data(env, client: influx.InfluxDBClient, table, stime, etime, cols_to_drop=None):
    #data = client.read_frame(iql.format(**locals()))
    data = client.read_frame(iql_all.format(**locals()))
    if cols_to_drop:
        data = data.drop(cols_to_drop, axis=1)
    # The fillna is necessary as some fields do have nan values and this is normal.
    return data.fillna(0)


datestr = sys.argv[1]
env = 'paperopt01'
stime = pandas.Timestamp(datestr, tz='UTC')
etime = stime + timedelta(days=6)
print(etime)
print(stime)
influx = influx.InfluxDBClient('options-influx')
data = _load_data(env, influx, 'event', stime, etime)
print('data')
print(data)
