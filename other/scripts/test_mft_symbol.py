import datetime
from octopus import tradeable 
from octopus import assets
from octopus import universe
import requests
import json

def get_service_location(consul_url: str, service_name: str) -> str:
    print(f"Fetching consul service location for {service_name}...\n")
    response = requests.get(f"{consul_url}:8500/v1/catalog/service/{service_name}")
    service_data = response.json()
    services = [f"http://{x['ServiceAddress']}:{x['ServicePort']}" for x in service_data]
    if len(services) == 0:
        raise Exception("Consul service definition request yielded no results")
    return services[0]

# Get instruments from refdata
refdata_api = get_service_location('http://10.1.0.18', 'refdata-api') + '/v1/tprd-instruments/matched?domain=MFT'
resp = requests.get(refdata_api)
if not resp.ok:
    print(f'Could not get instruments from {refdata_api}')

refdata_instruments = json.loads(resp.content)
refdata_instruments_mft_to_epoch = {inst['mftSymbol']:inst for inst in refdata_instruments }
refdata_instruments_epoch_to_mft = {inst['epochSymbol']:inst for inst in refdata_instruments }

# Live Assets
start_date = datetime.datetime.today().date()
traded = tradeable.get_trading_idents('live', start_date, start_date + datetime.timedelta(days=8))
for instrument in traded:
    asset = assets.get(instrument.symbol)
    if asset.epoch_symbol != asset.mft_symbol:
        print(f'MFT:{asset.mft_symbol} -- EPOCH:{asset.epoch_symbol}')
    if refdata_instruments_epoch_to_mft[asset.epoch_symbol] != refdata_instruments_mft_to_epoch[asset.mft_symbol]:
        print(f'Refdata warning for {asset.epoch_symbol} -- {asset.mft_symbol}')
        print(f'MFT: {refdata_instruments_mft_to_epoch[asset.mft_symbol]}')
        print(f'EPOCH: {refdata_instruments_epoch_to_mft[asset.epoch_symbol]}')

# Ref Assets
traded = tradeable.get_trading_idents('rel', start_date, start_date + datetime.timedelta(days=7))
for instrument in traded:
    asset = assets.get(instrument.symbol)
    if asset.epoch_symbol != asset.mft_symbol:
        print(f'MFT:{asset.mft_symbol} -- EPOCH:{asset.epoch_symbol}')
    if refdata_instruments_epoch_to_mft[asset.epoch_symbol] != refdata_instruments_mft_to_epoch[asset.mft_symbol]:
        print(f'Refdata warning for {asset.epoch_symbol} -- {asset.mft_symbol}')
        print(f'MFT: {refdata_instruments_mft_to_epoch[asset.mft_symbol]}')
        print(f'EPOCH: {refdata_instruments_epoch_to_mft[asset.epoch_symbol]}')

# Options Assets
options_traded = universe.zeno_universe_weekly('OESW')
for instrument in options_traded:
    asset = assets.get(instrument)
    if asset.epoch_symbol != asset.mft_symbol:
        print(f'MFT:{asset.mft_symbol} -- EPOCH:{asset.epoch_symbol}')
    if asset.mft_symbol not in refdata_instruments_mft_to_epoch:
        print(f'Refdata warning for {asset.mft_symbol}')
