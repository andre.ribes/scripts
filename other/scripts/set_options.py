import tabulate

from octopus import venues
from octopus import assets

es_option        = assets.get('ES4600I23')
es_weekly_option = assets.get('ES3W4625I23')

es_op_v_tt    = venues.get('TT', es_option)
es_op_v_rfa   = venues.get('RFA', es_option)
es_w_op_v_tt  = venues.get('TT', es_weekly_option)
es_w_op_v_rfa = venues.get('RFA', es_weekly_option)

table = [
    ['ES OPTION', es_option.longname, es_op_v_tt.ric, es_op_v_rfa.ric],
    ['ES WEEKLY OPTION', es_weekly_option.longname, es_w_op_v_tt.ric, es_w_op_v_rfa.ric],
]

print(tabulate.tabulate(table, ["Type", "MFT RIC", "TT RIC", "RFA RIC"]))
