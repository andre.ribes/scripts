#!/bin/bash

export MFT_BASE_DIR=/Data/mft
genrfaref 20230402 --options-csv=/Data/mft/md/rfa-option-rics/20230402.csv --strike-filter-cfg /home/andre.ribes/mft-dist/etc/strike_filter_config.yaml --position-file /Data/mft/trading/mfpositions/opt01-mfpositions/20230402.csv --output /home/andre.ribes/temp/20230402.yaml /home/andre.ribes/temp/20230402.xml
#genrfaref 20230326 --options-csv=/Data/mft/md/rfa-option-rics/20230326.csv --strike-filter-cfg /home/andre.ribes/mft-dist/etc/strike_filter_config.yaml --position-file /Data/mft/trading/mfpositions/opt01-mfpositions/20230326.csv --output /home/andre.ribes/temp/20230326.yaml /home/andre.ribes/temp/20230326.xml
#genrfaref 20230319 --options-csv=/Data/mft/md/rfa-option-rics/20230319.csv --strike-filter-cfg /home/andre.ribes/mft-dist/etc/strike_filter_config.yaml --position-file /Data/mft/trading/mfpositions/opt01-mfpositions/20230319.csv --output /home/andre.ribes/temp/20230319.yaml /home/andre.ribes/temp/20230319.xml
#genrfaref 20230312 --options-csv=/Data/mft/md/rfa-option-rics/20230312.csv --strike-filter-cfg /home/andre.ribes/mft-dist/etc/strike_filter_config.yaml --position-file /Data/mft/trading/mfpositions/opt01-mfpositions/20230312.csv --output /home/andre.ribes/temp/20230312.yaml /home/andre.ribes/temp/20230312.xml
#genrfaref 20230305 --options-csv=/Data/mft/md/rfa-option-rics/20230305.csv --strike-filter-cfg /home/andre.ribes/mft-dist/etc/strike_filter_config.yaml --position-file /Data/mft/trading/mfpositions/opt01-mfpositions/20230305.csv --output /home/andre.ribes/temp/20230305.yaml /home/andre.ribes/temp/20230305.xml

