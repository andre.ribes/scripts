#!/usr/bin/env bash
# See https://youtrack.jetbrains.com/issue/PY-35978/Support-conda-with-remote-interpreters
set -e

# Bring in mft-dist aliases
# shellcheck disable=SC1091
source "${HOME}/mft-dist/dotfiles/alias"
source "${HOME}/src/other/andre_aliases"

# Activate pnl env quietly
act options > /dev/null

# Defaults to unicorn repo, so we cd to the parent
cd ..

# Forward all args to active python
python "$@"
