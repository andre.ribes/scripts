#!/usr/bin/env bash
# See https://youtrack.jetbrains.com/issue/PY-35978/Support-conda-with-remote-interpreters
set -e

# Bring in mft-dist aliases
# shellcheck disable=SC1091
source "${HOME}/mft-dist/dotfiles/alias"
source "${HOME}/src/other/andre_aliases"

# Activate pnl env quietly
act rss > /dev/null

# Defaults to unicorn repo, so we cd to the parent
cd ${HOME}

export PYTHONPATH=/home/andre.ribes/var/src/rss:$PYTHONPATH

# Forward all args to active python
# export ASAN_OPTIONS=verify_asan_link_order=0:verbosity=1
python "$@"
