from __future__ import annotations

import numpy as np
import pandas as pd
import datetime


from epoch_markets import symlib
from epoch_markets.symlib import options, base

class OptionsRefDataConstants:
    """
    Constants for the options reference data.
    """
    columns = {
        'symbol': str,
        'base_asset': str,
        'underlying_symbol': str,
        'pc': str, # C or P
        'strike_price': float,
        'point_value': int,
        'expiry_date': 'datetime64[ns]',
        'expiry_timestamp': 'datetime64[ns]',
        'exercise_style': str,
        'settlement_type': str,
        'currency': str,
        'exchange': str,
        'timezone': str,
        'source': str,
        'strike_pt_multiplier': float,
        'price_pt_multipler': float
    }

    refdata = {
        'OESW': {
            'point_value': 50,
            'exercise_style': 'E',
            'settlement_type': 'Future',
            'timezone': 'America/Chicago',
            'exchange': 'CME',
            'strike_pt_multiplier': 1.0,
            'price_pt_multipler': 1.0
        },
        'OSPXW': {
            'point_value': 100,
            'exercise_style': 'E',
            'settlement_type': 'Cash',
            'timezone': 'America/Chicago',
            'exchange': 'CBOE',
            'strike_pt_multiplier': 1.0,
            'price_pt_multipler': 1.0
        },
        'OS0': {
            'point_value': 2500,
            'exercise_style': 'E',
            'settlement_type': 'Future',
            'timezone': 'America/Chicago',
            'exchange': 'CME',
            'strike_pt_multiplier': 0.01,
            'price_pt_multipler': 1.0
        },
        'OTY': {
            'point_value': 100000,
            'exercise_style': 'E',
            'settlement_type': 'Future',
            'timezone': 'America/Chicago',
            'exchange': 'CME',
            'strike_pt_multiplier': 1.0,
            'price_pt_multipler': 100.0
        },
        'OCL': {
            'point_value': 1000,
            'exercise_style': 'E',
            'settlement_type': 'Cash',
            'timezone': 'America/Chicago',
            'exchange': 'CME',
            'strike_pt_multiplier': 1.0,
            'price_pt_multipler': 1.0
        },
        'OC': {
            'point_value': 5000,
            'exercise_style': 'E',
            'settlement_type': 'Future',
            'timezone': 'America/Chicago',
            'exchange': 'CME',
            'strike_pt_multiplier': 1.0,
            'price_pt_multipler': 100.0
        },
        'OS': {
            'point_value': 5000,
            'exercise_style': 'E',
            'settlement_type': 'Future',
            'timezone': 'America/Chicago',
            'exchange': 'CME',
            'strike_pt_multiplier': 1.0,
            'price_pt_multipler': 100.0
        },
        'ONQ': {
            'point_value': 20,
            'exercise_style': 'E',
            'settlement_type': 'Future',
            'timezone': 'America/Chicago',
            'exchange': 'CME',
            'strike_pt_multiplier': 1.0,
            'price_pt_multipler': 1.0
        },
        'ORTY': {
            'point_value': 50,
            'exercise_style': 'E',
            'settlement_type': 'Future',
            'timezone': 'America/Chicago',
            'exchange': 'CME',
            'strike_pt_multiplier': 1.0,
            'price_pt_multipler': 1.0
        }
    }

    @staticmethod
    def currency_to_str(c: base.Currency):
        if c == base.Currency.USD:
            return 'USD'
        if c == base.Currency.AUD:
            return 'AUD'
        if c == base.Currency.EUR:
            return 'EUR'
        return 'NotDefined'

def underlying_symbol(secmaster: symlib.SecMaster, base_asset: str, expiry_date: datetime.date) -> str:
    if base_asset == "OESW":
        key = symlib.InstrumentKey("ES", expiry_date, 0)
        fut = secmaster.get_instrument(key)
        if fut is not None:
            return fut.exchange_symbol[:-1] + expiry_date.strftime("%y")
        else:
            return "not found"

    if base_asset == "OCL":
        key = symlib.InstrumentKey("CL", expiry_date, 0)
        fut = secmaster.get_instrument(key)
        if fut is not None:
            return fut.exchange_symbol[:-1] + expiry_date.strftime("%y")
        else:
            return "not found"

    if base_asset == "OTY":
        key = symlib.InstrumentKey("ZN", expiry_date, 0)
        fut = secmaster.get_instrument(key)
        if fut is not None:
            return fut.exchange_symbol[:-1] + expiry_date.strftime("%y")
        else:
            return "not found"

    if base_asset == "ONQ":
        key = symlib.InstrumentKey("NQ", expiry_date, 0)
        fut = secmaster.get_instrument(key)
        if fut is not None:
            return fut.exchange_symbol[:-1] + expiry_date.strftime("%y")
        else:
            return "not found"

    if base_asset == "OC":
        key = symlib.InstrumentKey("C", expiry_date, 0)
        fut = secmaster.get_instrument(key)
        if fut is not None:
            return fut.exchange_symbol[:-1] + expiry_date.strftime("%y")
        else:
            return "not found"

    if base_asset == "OS":
        key = symlib.InstrumentKey("S", expiry_date, 0)
        fut = secmaster.get_instrument(key)
        if fut is not None:
            return fut.exchange_symbol[:-1] + expiry_date.strftime("%y")
        else:
            return "not found"

    if base_asset == "ORTY":
        key = symlib.InstrumentKey("TFS", expiry_date, 0)
        fut = secmaster.get_instrument(key)
        if fut is not None:
            return fut.exchange_symbol[:-1] + expiry_date.strftime("%y")
        else:
            return "not found"

    if base_asset == "OSPXW":
        return ".INX"

    return "not known"

def get_refdata(secmaster: symlib.SecMaster, base_asset: str, options: list[options.OptionInstrument]) -> pd.DataFrame:
    refdata = pd.DataFrame(options)
    refdata = refdata.rename(columns={'exchange_symbol' : 'symbol', 'expiration_date' : 'expiry_date', 'put_or_call' : 'pc'})

    # Common for all base assets
    refdata['base_asset'] = base_asset
    refdata['source'] = 'pcap'
    refdata['pc'] = np.where(refdata.pc == 1, 'C', 'P')
    refdata['currency'] = refdata['currency'].apply(lambda c: OptionsRefDataConstants.currency_to_str(c))
    refdata['expiry_timestamp'] = refdata.expiry_date
    refdata['expiry_date'] = refdata.expiry_date.apply(lambda d: d.date())
    refdata['underlying_symbol'] = refdata['expiry_date'].apply(lambda r: underlying_symbol(secmaster, base_asset, r))

    for c_name, c_type in OptionsRefDataConstants.columns.items():
        if c_name in OptionsRefDataConstants.refdata[base_asset]:
            refdata[c_name] = OptionsRefDataConstants.refdata[base_asset][c_name]
        refdata[c_name] = refdata[c_name].astype(c_type)

    if base_asset == 'OESW':
        refdata = refdata[refdata.product_symbol != 'ES']
    if base_asset == 'OSPXW':
        refdata = refdata[refdata.product_symbol.str.match('SPXW')]
    elif base_asset == 'OS0':
        refdata = refdata[refdata.product_symbol.str.match('S0[1-5]')]
    elif base_asset == 'OTY':
        refdata = refdata[
           (refdata.product_symbol.str.match('VY[1-5]')) # Monday
         | (refdata.product_symbol.str.match('WY[1-5]')) # Wednesday
         | (refdata.product_symbol.str.match('ZN[1-5]')) # Friday
         ]
    elif base_asset == 'OCL':
        refdata = refdata[
            (refdata.product_symbol.str.match('ML[1-5]')) # Monday
          | (refdata.product_symbol.str.match('WL[1-5]')) # Wednesday
          | (refdata.product_symbol.str.match('LO[1-5]')) # Friday
          ]
    elif base_asset == 'OC':
        refdata = refdata[
            (refdata.product_symbol.str.match('ZC[1-5]')) # Friday
          | (refdata.product_symbol.str.match('OZC'))     # Monthly - American Option
          ]
    elif base_asset == 'OS':
        refdata = refdata[
            (refdata.product_symbol.str.match('ZS[1-5]')) # Friday
          ]
    elif base_asset == 'ONQ':
        refdata = refdata[
            (refdata.product_symbol.str.match('Q[1-5]A')) # Monday
          | (refdata.product_symbol.str.match('Q[1-5]B')) # Tuesday
          | (refdata.product_symbol.str.match('Q[1-5]C')) # Wednesday
          | (refdata.product_symbol.str.match('Q[1-5]D')) # Thursday
          | (refdata.product_symbol.str.match('QN[1-5]')) # Friday
          | (refdata.product_symbol.str.match('QNE')) # Monthly
          ]
    elif base_asset == 'ORTY':
        refdata = refdata[
            (refdata.product_symbol.str.match('R[1-5]A')) # Monday
          | (refdata.product_symbol.str.match('R[1-5]B')) # Tuesday
          | (refdata.product_symbol.str.match('R[1-5]C')) # Wednesday
          | (refdata.product_symbol.str.match('R[1-5]D')) # Thursday
          | (refdata.product_symbol.str.match('R[1-5]E')) # Friday
          | (refdata.product_symbol.str.match('RTM')) # Monthly
          ]
    else:
        AssertionError(f'Base asset {base_asset} not supported')

    refdata = refdata.drop(columns=['instrument_key', 'exchange', 'product_symbol', 'group_symbol', 'activation_date', 'display_factor', 'tick_size', 'term', 'channel'])
    return refdata