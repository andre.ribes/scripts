from __future__ import annotations

import pandas as pd
import numpy

from epoch_data import hdstorelib, calendarlib
from epoch_markets import symlib
from epoch_markets.symlib import options, base

def get_http_secmaster(host: str = "research-web.service.nwk1.consul") -> symlib.SecMaster:
    data_client = hdstorelib.MainDataClient(
        protocol="http",
        host=host,
        name_resolver=hdstorelib.DNSResolver(),
        network_latency=None)

    fronts_factory = symlib.FrontsFactory(
        data_client=data_client,
        base_path="secdef",
        front_scheme="mft_rel",
        ignore_missing_products=True,
    )

    secmaster = symlib.SecMaster(
        data_client=data_client,
        base_path="secdef",
        fronts_factory=fronts_factory,
    )

    return secmaster

def get_shared_fs_secmaster(path_prefix: str = "/Data") -> symlib.SecMaster:
    data_client = hdstorelib.MainDataClient(
        protocol = "file",
        host=None,
        name_resolver=None,
        network_latency=None,
        path_prefix=path_prefix)

    fronts_factory = symlib.FrontsFactory(
        data_client=data_client,
        base_path="secdef",
        front_scheme="mft_rel",
        ignore_missing_products=True,
    )

    secmaster = symlib.SecMaster(
        data_client=data_client,
        base_path="secdef",
        fronts_factory=fronts_factory,
    )

    return secmaster

def get_known_option_gsyms():
    options_sym = [k for k,v in base.InstrumentKey.internal_to_instrument_mapping.items() if v.instrument_type == base.InstrumentType.Option]
    return options_sym

def get_option_chain(
    gsym: str, trade_date: calendarlib.DateLike, secmaster: symlib.SecMaster
) -> list[options.OptionInstrument]:
    # This is help the compatibility between OESW (mft) and OES (secmaster)
    if gsym == 'OESW':
        gsym = 'OES'
    o_key = options.OptionInstrumentKey(gsym, trade_date)
    return secmaster.get_option_chain(o_key, last_eligible_trade_date=o_key.date)
